# Shopping basket
from math import floor
from collections import defaultdict
from decimal import Decimal, getcontext

getcontext().prec = 2

# Our database
class Products(object):
    '''
    Pseudo-database
    '''

    def __init__(self):
        self._active_offers = {
            'Baked Beans': '2 for 1',
            'Sardines': '25% discount'
        }
        self._products = {
            'Baked Beans': {'price': 0.99, 'class': ['tinned']},
            'Biscuits': {'price': 1.20, 'class': ['baking']},
            'Sardines': {'price': 1.89, 'class': ['fish', 'tinned']},
            'Shampoo (small)': {'price': 2.00, 'class': ['toiletries', 'shampoo']},
            'Shampoo (medium)': {'price': 2.50, 'class': ['toiletries', 'shampoo']},
            'Shampoo (large)': {'price': 3.50, 'class': ['toiletries', 'shampoo']}
        }

    def get_product(self, product_id):
        if product_id not in self._products.keys():
            return
        return self._products[product_id]

    def all_products(self):
        return self._products

    def get_active_offers(self):
        '''
        Would be a database hit or some other way of mapping offer to product
        Ideally, we would use proper keys, but this is good enough for now
        '''
        return self._active_offers


class Pricer(object):
    def __init__(self):
        self._total = 0.0
        self._discount = 0.0
        self._qtty = 1
        self._price = 1.0
        self._pre_discount = self._price

    def _calculate(self):
        self._pre_discount = self._price * self._qtty

    @property
    def sub_total(self):
        return float(f'{self._pre_discount:.2f}')

    @property
    def final_price(self):
        return float(f'{self._total:.2f}')

    @property
    def discount_applied(self):
        return float(f'{self._discount}')

    @property
    def qtty(self):
        return self._qtty

    @qtty.setter
    def qtty(self, newVal: int = 0):
        if newVal < 0:
            raise ValueError('Quantity cannot be less than zero!')
        self._qtty = newVal
        self._calculate()

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, newVal: float = 1.0):
        if newVal <= 0.0:
            raise ValueError('invalid price')
        self._price = newVal
        self._calculate()


class X_for_y(Pricer):
    '''
    Calculate the 2 for 1 type of price discount
    '''

    def __init__(self, x: int = 2, y: int = 2):
        super().__init__()
        self._x = x
        self._y = y
        self._qtty = 0
        self._price = 1.0

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, newVal: int = 0):
        if newVal < 1:
            raise ValueError('y value cannot be less than 1')
        self._y = newVal
        self._calculate()

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, newVal: int = 2):
        if newVal < 1:
            raise ValueError('x value cannot be less than 1')
        self._x = newVal
        self._calculate()

    def _calculate(self):
        '''
        Calculation function
        is only called when we want either a price or a discount
        '''
        quantity = self._qtty
        if quantity <= 0:
            self._total = 0.0
            self._discount = 0.0
            return
        x_for, remainder = divmod(quantity, self._x)
        qtty_to_charge = (x_for * self._y) + remainder
        self._pre_discount = quantity * self._price
        self._total = qtty_to_charge * self._price
        self._discount = (quantity * self._price) - self._total
        if self._total <= 0.0:
            self._total = 0.0


class PercentDiscount(Pricer):
    '''
    Calculate percentage discounts
    '''

    def __init__(self, percent_as_whole_number: int = 5, price: float = 1.0):
        super().__init__()
        if percent_as_whole_number > 100:
            raise ValueError('Cannot discount greater than the value of the product')
        if percent_as_whole_number <= 0:
            raise ValueError('Cannot use zero as a percentage')
        self._percent = percent_as_whole_number / 100.0

    def _calculate(self):
        quantity = self._qtty
        if quantity <= 0:
            self._total = 0.0
            self._discount = 0.0
            return
        self._pre_discount = quantity * self._price
        discount_to_apply = (self._percent * self._price) * quantity
        self._discount = discount_to_apply
        self._total = self._pre_discount - discount_to_apply

    @property
    def percent(self):
        # take the lower bound
        return floor(self._percent * 100)

    @percent.setter
    def percent(self, newVal: int = 5):
        if newVal > 100:
            raise ValueError('Cannot discount greater than the value of the product')
        if newVal <= 0:
            raise ValueError('Cannot use zero as a percentage')
        self._percent = newVal / 100.0


class Offers(object):
    '''
    Here we map offers to specific implementations of the Discount classes
    '''

    def __init__(self):
        self.offers = {
            '2 for 1': X_for_y(2, 1),
            '3 for 2': X_for_y(3, 2),
            '5% discount': PercentDiscount(5),
            '25% discount': PercentDiscount(25)
        }

    def offer(self, offer):
        if offer not in self.offers.keys():
            raise ValueError('Invalid offer')
        return self.offers[offer]

    def add_offer(self, new_offer, instance):
        '''
        Add or update our offer dictionary with a new offer
        '''
        self.offers[new_offer] = instance

    def remove_offer(self, offer):
        if offer not in self.offers.keys():
            raise ValueError('Invalid offer title')
        del self.offers[offer]


class Basket(object):
    def __init__(self):
        self._basket = defaultdict(int)
        self._sub_total = 0.0
        self._final_total = 0.0
        self._total_discount = 0.0

    @property
    def sub_total(self):
        return self._sub_total

    @property
    def final_total(self):
        return self._final_total

    @property
    def total_discount(self):
        return self._total_discount

    @property
    def basket(self):
        return self._basket

    @property
    def products(self):
        return sum([x for x in self._basket.values()])

    @property
    def discount(self):
        return float(f'{self._total_discount:.2f}')

    def _calculate_discount(self):
        '''
        Calculate total discount applied to basket
        '''
        # create an instance of offers - would be a database query most likely
        offers = Offers()
        products = Products()
        active_offers = products.get_active_offers()
        self._sub_total = 0
        self._final_total = 0
        self._total_discount = 0
        for k, v in self._basket.items():
            product = products.get_product(k)
            if not product:
                continue
            if k in active_offers:
                offer_title = active_offers[k]
                offer = offers.offer(offer_title)
                offer.qtty = v
                # given time, product would most likely be a proper class object
                offer.price = product['price']
                self._sub_total += offer.sub_total
                self._total_discount += offer.discount_applied
                self._final_total += offer.final_price
            else:
                ttl = v * product['price']
                self._sub_total += ttl
                self._final_total += ttl
        # Calculate ourselves down to zero!
        if self._sub_total <= 0.0:
            self._sub_total = 0
        if self._final_total <= 0.0:
            self._final_total = 0

    def add_item(self, product):
        '''
        Get our product from the given product ID - if exists we add it to our basket
        '''
        products = Products()
        product_data = products.get_product(product)
        if not product_data:
            raise ValueError('Product does not exist')
        self._basket[product] += 1
        self._calculate_discount()

    def remove_item(self, product):
        if product in self._basket:
            self._basket[product] -= 1
            if self._basket[product] <= 0:
                del self._basket[product]
        else:
            raise ValueError('Product not in basket')
        self._calculate_discount()


def main():
    import random
    basket = Basket()
    products = Products()

    for k in products.all_products().keys():
        qtty = random.randint(1, 5)
        for i in range(0, qtty):
            basket.add_item(k)
    for product, qtty in basket.basket.items():
        product_data = products.get_product(product)
        print(f'{qtty} x {product} ({product_data["price"]:0.2f})')
    print(f'sub-total: {basket.sub_total:0.2f}')
    print(f'discount {basket.discount:0.2f}')
    print(f'final-total {basket.final_total:0.2f}')


if __name__ == '__main__':
    main()
