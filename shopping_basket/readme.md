## Shopping Cart

## Discount Strategies

Basic shopping cart implementation with discounts. Two discount strategies created for sample:

* X_for_y
* PercentageDiscount

They both implement the basic Pricer class interface, and provide: qtt, price, final_price, sub_total and
discount_applied Use them as references when creating further discount strategies

## Offers

The `Offers` class maps string representation of offers to their respective instance of their discount. Included are
four sample mappings to get you started:

* '2 for 1'
* '3 for 2'
* '5% discount'
* '25% discount'

To change the exsting implementation or to add a new offer, call the Offers class with the offer title and instanced
Discount class - see Offers class for more information

## Products

The Products class provides a simple interface which (right now) uses a dictionary to return pricing data for a product.
This could easily be extended to use a database instance if required. Products require a price field in the return
dictionary. The interface of the class also has the mapping between products and offers. If you wished to have multiple
simultaneous offers on a product, then there would be refactoring required so that the active offers would be stored
as `key: [list of offers]`. Right now, they are just string mappings, e.g.

```json
{
    'Baked Beans': '2 for 1', 
    'Sardines': '25% discount'
}
```

## Basket

The basket is where the meat of the work is done, when an item is added or removed from the cart, the discounts and
totals are recalculated

# Usage

Import Basket library as required

```python
from basket import Basket
basket = Basket()
products = Products()
basket.add_item('Baked Beans')
print(f'sub total: {basket.sub_total}')
print(f'discount: {basket.discount}')
print(f'final total: {basket.final_total}')

```


