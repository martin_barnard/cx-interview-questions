import unittest

import pytest

from ..basket import Basket


class BasketCase(unittest.TestCase):
    def test_empty_basket(self):
        basket = Basket()
        assert basket.products == 0

    def test_add_one_item(self):
        basket = Basket()
        product = 'Biscuits'
        basket.add_item(product)
        assert basket.products == 1

    def test_item_total_correct(self):
        basket = Basket()
        product = 'Biscuits'
        basket.add_item(product)
        assert list(basket.basket.keys()) == ['Biscuits']
        assert list(basket.basket.values()) == [1]

    def test_item_no_discount_correct(self):
        basket = Basket()
        product = 'Biscuits'
        basket.add_item(product)
        assert basket.final_total == 1.20

    def test_item_with_discounted_offer_25_percent(self):
        basket = Basket()
        product = 'Sardines'
        basket.add_item(product)
        # Rounding up
        assert basket.final_total == 1.42

    def test_mixed_basket(self):
        '''
        Our current active offers are baaked beans on 2 for 1 and sardines at 25%
        '''
        basket = Basket()
        products = ['Biscuits', 'Biscuits', 'Baked Beans', 'Sardines', 'Sardines', 'Baked Beans']
        # Should total 1.2 + 1.2 + 0.99 + 1.89 + 1.89 + 0.99 before discounts (8.16)
        # Discounts are : 2 for 1 on beans -0.99 and 25% on sardines (-0.4725 per tin)
        # Total discount is (2 * -0.4725 = -0.945) + (-0.99) = -1.935
        # Final total is: 8.16 - 1.935 = 6.225
        for item in products:
            basket.add_item(item)
        # Rounding up
        assert basket.sub_total == 8.16
        assert basket.discount == 1.94
        assert basket.final_total == 6.22

    def test_invalid_basket_item(self):
        basket = Basket()
        products = ['does not exist']
        for item in products:
            with pytest.raises(ValueError) as e:
                basket.add_item(item)
                assert e.value.args[0] == 'Product does not exist'

    def test_remove_item(self):
        '''
        Add 4 sardines and remove 3 of them.
        Should be 25% discount, which is 1.89 - 0.4725 = 1.4175 * 3 or 4.25249999999
        '''
        basket = Basket()
        product = 'Sardines'
        for i in range(0, 4):
            basket.add_item(product)
        basket.remove_item(product)
        # Rounding up
        assert basket.final_total == 4.25
        assert basket.sub_total == 5.67

    def test_remove_all_items(self):
        '''
        Remove all items from a basket & have zeroes
        '''
        basket = Basket()
        product = 'Sardines'
        basket.add_item(product)
        basket.add_item(product)
        basket.add_item(product)
        assert basket.final_total == 4.25
        assert basket.sub_total == 5.67
        assert basket.discount == 1.42
        basket.remove_item(product)
        basket.remove_item(product)
        basket.remove_item(product)
        assert basket.final_total == 0.0
        assert basket.sub_total == 0
        assert basket.discount == 0

    def test_remove_extra_item(self):
        '''
        Remove extra item from our basket
        '''
        basket = Basket()
        product = 'Sardines'
        basket.add_item(product)
        basket.remove_item(product)
        with pytest.raises(ValueError) as e:
            basket.remove_item(product)
            assert e.value.args[0] == 'Product not in basket'
        assert basket.final_total == 0.0
        assert basket.sub_total == 0
        assert basket.discount == 0


if __name__ == '__main__':
    unittest.main()
